package models

import (
	"encoding/json"
	"net/http"
	"time"

	"github.com/lukaszbartminski/url-collector/libs/date"
	"github.com/lukaszbartminski/url-collector/libs/url"
)

type Picture struct {
	URL string `json:"url"`
}

type Response interface {
	new()
}

type OKResponse struct {
	URLs []string `json:"urls"`
}

func (okresp OKResponse) new() {}

type ErrorResponse struct {
	Error string `json:"error"`
}

func (errresp ErrorResponse) new() {}

func GetByRange(startDate string, endDate string, sem chan int) (response Response, status int) {
	errRes := ErrorResponse{}
	okRes := OKResponse{}
	err := date.VerifyRange(startDate, endDate)

	switch err.(type) {
	case *date.EndDateBeforeStart:
		errRes.Error = err.Error()
		return errRes, http.StatusBadRequest
	case *time.ParseError:
		errRes.Error = err.Error()
		return errRes, http.StatusBadRequest
	case *date.DateInFuture:
		errRes.Error = err.Error()
		return errRes, http.StatusBadRequest
	case *date.EndDateEqualStart: // Probably that shouldn't be the error
		pic, err := getOne(startDate, sem)
		if err != nil {
			errRes.Error = err.Error()
			return errRes, http.StatusInternalServerError
		}
		okRes.URLs = append(okRes.URLs, pic.URL)
		return okRes, http.StatusOK
	case nil:
		pics, err := getMany("", startDate, endDate, sem)
		if err != nil {
			errRes.Error = err.Error()
			return errRes, http.StatusInternalServerError
		}
		for _, pic := range pics {
			okRes.URLs = append(okRes.URLs, pic.URL)
		}
		return okRes, http.StatusOK
	}
	return errRes, http.StatusInternalServerError
}

func getOne(date string, sem chan int) (picture Picture, err error) {
	sem <- 1
	pic := Picture{}
	nB := url.Builder{}
	resp, err := http.Get(nB.NASA(date))
	if err != nil {
		<-sem
		return pic, err
	}
	json.NewDecoder(resp.Body).Decode(&pic)
	<-sem
	return pic, nil
}

func getMany(url string, startDate string, endDate string, sem chan int) (pictures []Picture, err error) {
	pics := []Picture{}
	start, err := date.StringToDate(startDate)
	if err != nil {
		return pics, err
	}
	end, err := date.StringToDate(endDate)
	/*
	* I am not sure I should test err here - if codes got into here, there is no way to have error now,
	* because startDate and endDate values are already checked in GetRange function. Although, I would leave
	* err checking here, because maybe in future some other func will use this function and I can't assume now,
	* this future func will check for startDate and endDate corectness
	 */
	if err != nil {
		return pics, err
	}
	for d := start; d.After(end) == false; d = d.AddDate(0, 0, 1) {
		pic, err := getOne(d.Format("2006-01-02"), sem)
		if err != nil {
			return pics, err
			// In real life scenario, I shouldnt stop this loop when got error here, because
			// following iterations might return legit pic
		}
		pics = append(pics, pic)
	}
	return pics, nil
}
