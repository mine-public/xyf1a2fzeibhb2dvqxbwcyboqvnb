package pictures

import (
	"github.com/gin-gonic/gin"
	"github.com/lukaszbartminski/url-collector/models"
)

func GetByRange(sem chan int) gin.HandlerFunc {
	return func(c *gin.Context) {
		start := c.Query("start_date")
		end := c.Query("end_date")

		res, status := models.GetByRange(start, end, sem)
		c.JSON(status, res)
	}
}
