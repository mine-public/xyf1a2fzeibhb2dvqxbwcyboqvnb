package main

import (
	"fmt"
	"os"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/lukaszbartminski/url-collector/controllers/pictures"
	"github.com/spf13/viper"
)

func main() {
	viper.SetConfigName("app")
	viper.SetConfigType("env")
	viper.AddConfigPath(".")
	viper.AutomaticEnv()
	err := viper.ReadInConfig()
	if err != nil {
		fmt.Println("fatal error config file: default \n", err)
		os.Exit(1)
	}
	os.Setenv("API_KEY", viper.GetString("API_KEY"))
	os.Setenv("CONCURRENT_REQUESTS", viper.GetString("CONCURRENT_REQUESTS"))
	os.Setenv("PORT", viper.GetString("PORT"))

	cr, err := strconv.Atoi(os.Getenv("CONCURRENT_REQUESTS"))
	if err != nil {
		fmt.Println(err)
		return
	}
	sem := make(chan int, cr)

	r := gin.Default()

	r.GET("/pictures", pictures.GetByRange(sem))

	r.Run()
}
