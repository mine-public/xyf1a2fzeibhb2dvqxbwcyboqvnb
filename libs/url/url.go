package url

import (
	"fmt"
	"os"
)

type Builder struct{}

func (b *Builder) NASA(date string) string {
	return fmt.Sprintf("https://api.nasa.gov/planetary/apod?api_key=%v&date=%v", os.Getenv("API_KEY"), date)
}
