package date

import (
	"strings"
	"testing"
	"time"
)

func TestVerifyRange(t *testing.T) {
	errEndBeforeStart := EndDateBeforeStart{}
	errFuture := DateInFuture{}
	errEndEqStart := EndDateEqualStart{}
	testCases := []struct {
		description string
		startDate   string
		endDate     string
		expected    string
	}{
		{"returns error due to endDate earlier than startDate", "2020-02-02", "2020-02-01", errEndBeforeStart.Error()},
		{"returns error due to wrong date format", "2020 Feb 01", "2020-02-01", "cannot parse"},
		{"returns error due to startDate and endDate are in future", "2021-02-02", "2021-02-01", errFuture.Error()},
		{"returns error due to startDate and endDate are the same", "2015-02-01", "2015-02-01", errEndEqStart.Error()},
	}
	for _, testCase := range testCases {
		t.Run(testCase.description, func(t *testing.T) {
			if result := VerifyRange(testCase.startDate, testCase.endDate); !strings.Contains(result.Error(), testCase.expected) {
				t.Errorf("Incorrect value, got %v, expected: %v", result.Error(), testCase.expected)
			}
		})
	}

	testCasesNoError := []struct {
		description string
		startDate   string
		endDate     string
		expected    error
	}{
		{"returns nil - range and dates are correct", "2020-02-01", "2020-02-02", nil},
	}
	for _, testCaseeNoError := range testCasesNoError {
		t.Run(testCaseeNoError.description, func(t *testing.T) {
			if result := VerifyRange(testCaseeNoError.startDate, testCaseeNoError.endDate); result != testCaseeNoError.expected {
				t.Errorf("Incorrect value, got %v, expected: %v", result.Error(), testCaseeNoError.expected)
			}
		})
	}
}

func TestStringToDate(t *testing.T) {
	testCases := []struct {
		description string
		date        string
		expected    time.Time
	}{
		{"returns properly parsed time value", "2020-02-01", time.Date(2020, 02, 01, 0, 0, 0, 0, time.UTC)},
		{"returns zero time value due to wrong value", "test", time.Date(1, 01, 01, 0, 0, 0, 0, time.UTC)},
	}
	for _, testCase := range testCases {
		t.Run(testCase.description, func(t *testing.T) {
			if result, _ := StringToDate(testCase.date); result != testCase.expected {
				t.Errorf("Incorrect value, got %v, expected: %v", result, testCase.expected)
			}
		})
	}
}
