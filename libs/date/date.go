package date

import (
	"time"
)

type EndDateBeforeStart struct{}

func (r *EndDateBeforeStart) Error() string {
	return "endDate before startDate"
}

type EndDateEqualStart struct{}

func (r *EndDateEqualStart) Error() string {
	return "endDate is equal startDate"
}

type DateInFuture struct{}

func (r *DateInFuture) Error() string {
	return "one/both of the dates is/are in the future"
}

func VerifyRange(startDate string, endDate string) error {
	start, err := StringToDate(startDate)
	if err != nil {
		return &time.ParseError{}
	}
	end, err := StringToDate(endDate)
	if err != nil {
		return &time.ParseError{}
	}
	switch {
	case start.After(time.Now()) || end.After(time.Now()):
		return &DateInFuture{}
	case end.Before(start):
		return &EndDateBeforeStart{}
	case start.Equal(end): // Probably that shouldn't be the error
		return &EndDateEqualStart{}
	}
	return nil
}

func StringToDate(date string) (time.Time, error) {
	layout := "2006-01-02" //Would be usefull to customize, but dont want to make an overkill

	t, err := time.Parse(layout, date)
	if err != nil {
		return t, err
	}
	return t, nil
}
